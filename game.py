"""===============================================
Goblin Dice Duel by Eric Wideman
==============================================="""

import random

# Takes in 2 rolls and returns 0,1,2
def rollCompare(rollA, rollB):
    if rollA > rollB:
        return 1 # rollA wins
    elif rollA < rollB:
        return 2 # rollB wins
    else:
        return 0 # tie
